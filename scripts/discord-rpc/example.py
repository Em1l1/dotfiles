import rpc
import subprocess
import time
from time import mktime

print("Mpd/NCmpcpp")
client_id = '782651784559984680'  # Your application's client ID as a string. (This isn't a real client ID)
rpc_obj = rpc.DiscordIpcClient.for_platform(client_id)  # Send the client ID to the rpc module
print("RPC connection successful.")
cover_dico =    {"Maximum Overload (Japanese Special Edition)" : "2014_-_maximum_overload",
                "Sonic Firestorm (Japanese Edition)" : "2004_-_sonic_firestorm",
                "Inhuman Rampage (Japanese Edition)" : "2006_-_inhuman_rampage",
                "All Good Songs (Compilation)" : "2016_-_all_good_songs",
                "Machines" : "machines",
                "Doom" : "doom_by_mick_gordon",
                "Lux" : "2013_-_lux",
                "Memento Mori" : "2016_-_memento_mori",
                "Metal Gear Rising Revengeance Vocal Tracks" : "metal_gear_rising_revengeance_vocal_tracks",
                "NieR:Automata Original Soundtrack" : "nier_automata",
                "Bloodborne Original Soundtrack" : "bloodborne",
                "Bloodborne Mini Soundtrack" : "bloodborne",
                "To the Moon <OST>" : "to_the_moon",
                "Finding Paradise <OST>" : "finding_paradise",
                "The Gereg (Deluxe Edition)" : "the_gereg"
                }

byArtist_cover_dico =   {"Linkin Park": "linkinpark",
                         "System Of A Down": "soad",
                         "System of a Down": "soad"
                        }



class Music:
    
    def __init__(self):
        self.name = "Music"
        self.artist = "Artist"
        self.album = "Album"
        self.timecode = "00:00/00:00"
        self.cover = "default"
    def setAll(self, name, artist, album, timecode, cover):
        self.name = name
        self.artist = artist
        self.album = album
        self.timecode = timecode
        self.cover = cover
    def updateTimecode(self, timecode):
        self.timecode = timecode


def musicFormat(raw_data,music,end_time):
    ret=0
    try:
        line_data = raw_data.decode("utf-8").split("\n");
        #line_data = str(raw_data)
        first_line_data = line_data[0][1:-1].split(';')
        second_line_data = line_data[1].split(' ')
        if second_line_data[0] == "[paused]":
            timecode = "⏸︎ " + second_line_data[5]
            ret=1

        else:
            timecode = "► " + second_line_data[4]
    except:
        exit()

    if(first_line_data[0]==music.name):
        music.updateTimecode(timecode)
        return end_time+ret
    #sep_time = first_line_data[3].split(':')
    #time=int(sep_time[0])*60+int(sep_time[1]) #we supose sep_time[0] = minutes sep_time = seconds
    time = endTime(second_line_data)

    if first_line_data[1] in byArtist_cover_dico:
        cover = byArtist_cover_dico[first_line_data[1]]
    elif first_line_data[2] in cover_dico:
        cover = cover_dico[first_line_data[2]]
    else:
        cover = "default"

    music.setAll(first_line_data[0], first_line_data[1], first_line_data[2], timecode, cover)
    return time

def endTime(second_line_data):
    if second_line_data[0] == "[paused]":
        sid = 5
    else:
        sid = 4
    sep_time=second_line_data[sid].split('/')[0].split(':')
    sep_time_end=second_line_data[sid].split('/')[1].split(':')
 
    offset_time=int(sep_time_end[0])*60+int(sep_time_end[1])-(int(sep_time[0])*60+int(sep_time[1]))#we supose sep_time[0] = minutes sep_time = seconds
    return mktime(time.localtime()) + offset_time


music = Music()
#start_time = mktime(time.localtime())
end_time = 0
while True:
    end_time = musicFormat(subprocess.Popen(["mpc","-f","\"%title%;%artist%;%album%;%time%\""], stdout=subprocess.PIPE).communicate()[0], music, end_time)
    
    activity = {
            "state": music.timecode,  # anything you like
            "details": music.name,  # anything you like
            "timestamps": {
                #"start": start_time
                "end": end_time
            },
            "assets": {
                "small_text": music.artist,  # anything you like
                "small_image": "artist",  # must match the image key
                "large_text": music.album,  # anything you like
                "large_image": music.cover# must match the image key
            }
        }
    rpc_obj.set_activity(activity)
    time.sleep(1)
