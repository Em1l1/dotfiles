"-----GENERAL-----
set clipboard=unnamedplus
set mouse=a

set hidden
set shortmess+=I
set nobackup
set noswapfile
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set smartindent
set autoindent
set showmatch
set scrolloff=8
set relativenumber
set number
set expandtab
set tabstop=4 softtabstop=4
set shiftwidth=4
set splitbelow
set splitright
set cursorline
set path+=**
set wildmenu

syntax on

let g:netrw_banner=0
" let g:netrw_liststyle=3


"-----SHORTCUTS-----
imap <C-c> <Esc>
let mapleader = " "

" Shortcutting split navigation
noremap <silent> <A-h> <C-w>h
noremap <silent> <A-j> <C-w>j
noremap <silent> <A-k> <C-w>k
noremap <silent> <A-l> <C-w>l

" Tab shortcuts
noremap <silent> <A-p> :tabp<CR>
noremap <silent> <A-n> :tabn<CR>
noremap <silent> <A-&> 1gt
noremap <silent> <A-é> 2gt
noremap <silent> <A-"> 3gt
noremap <silent> <A-'> 4gt
noremap <silent> <A-(> 5gt
noremap <silent> <A-§> 6gt
noremap <silent> <A-è> 7gt
noremap <silent> <A-!> 8gt
noremap <silent> <A-ç> 9gt
noremap <silent> <A-à> 10gt

noremap <silent> <A-t> :tabnew<CR>

" Open Ranger
noremap <silent> <A-o> :Ranger<CR>

" Toggle Nerdtree
noremap <silent> <C-n> :NvimTreeToggle<CR>

inoremap <C-l> <right>
map gf :edit <cfile><CR>

"-----Theme-----
colorscheme onedark
hi Normal guibg=NONE ctermbg=NONE
set showtabline=1  " Show tabline
sign define DiagnosticSignError text=❌ texthl=DiagnosticSignError linehl= numhl=
sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl=
sign define DiagnosticSignInfo text=כֿ texthl=DiagnosticSignInfo linehl= numhl=
sign define DiagnosticSignHint text=💡 texthl=DiagnosticSignHint linehl= numhl=

"-----PLUGINS-----
" Plugged
call plug#begin('~/.config/nvim/plugged')
Plug 'lilydjwg/colorizer'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'windwp/nvim-autopairs'
" Autocomplete + lsp
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp', { 'branch': 'main' }
Plug 'hrsh7th/cmp-buffer', { 'branch': 'main' }
Plug 'hrsh7th/cmp-path', { 'branch': 'main' }
Plug 'hrsh7th/cmp-cmdline', { 'branch': 'main' }
Plug 'hrsh7th/nvim-cmp', { 'branch': 'main' } 
call plug#end()
luafile ~/.config/nvim/lua/plugins/nvim-autopairs.lua
luafile ~/.config/nvim/lua/plugins/nvim-tree.lua
luafile ~/.config/nvim/lua/plugins/lsp-config.lua
set completeopt=menu,menuone,noselect
luafile ~/.config/nvim/lua/plugins/nvim-cmp.lua


cnoremap <silent> w!! execute 'silent! write !doas tee % >/dev/null' <bar> edit!
